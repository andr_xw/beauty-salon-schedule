import React from 'react'
import './modal.css'

class Modal extends React.Component {
    render() {
        return (
            <div className={this.props.show ? 'my-modal active' : 'my-modal'} onClick={this.props.changeShow}>
                <div className={this.props.show ? 'my-modal-content active' : 'my-modal-content'}
                     onClick={event => event.stopPropagation()}>
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default Modal;
