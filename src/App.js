import React from 'react'
import DayOfWeek from './DayOfWeek/DayOfWeek'
import Modal from './Modal/Modal'

class App extends React.Component {
  state = {
    dayofweek: [
      {
        name: 'Понедельник',
        active: true,
        list: [
          {
            name: 'В Москве',
            active: true,
            times: [
              {
                from: '09:00',
                to: '18:00'
              }
            ]
          },
          {
            name: 'Студия на Академической',
            active: true,
            times: [
              {
                from: '10:00',
                to: '19:00'
              }
            ]
          },
          {
            name: 'Перерыв',
            active: true,
            times: [
              {
                from: '14:00',
                to: '15:00'
              }
            ]
          }
        ]
      },
      {
        name: 'Вторник',
        active: false,
        list: [
          {
            name: 'В Москве',
            active: false,
            times: [
              {
                from: '09:00',
                to: '18:00'
              }
            ]
          },
          {
            name: 'Студия на Академической',
            active: false,
            times: [
              {
                from: '10:00',
                to: '19:00'
              }
            ]
          },
          {
            name: 'Перерыв',
            active: false,
            times: [
              {
                from: '14:00',
                to: '15:00'
              }
            ]
          }
        ]
      },
      {
        name: 'Среда',
        active: false,
        list: [
          {
            name: 'В Москве',
            active: false,
            times: [
              {
                from: '09:00',
                to: '18:00'
              }
            ]
          },
          {
            name: 'Студия на Академической',
            active: false,
            times: [
              {
                from: '10:00',
                to: '19:00'
              }
            ]
          },
          {
            name: 'Перерыв',
            active: false,
            times: [
              {
                from: '14:00',
                to: '15:00'
              }
            ]
          }
        ]
      },
      {
        name: 'Четверг',
        active: true,
        list: [
          {
            name: 'В Москве',
            active: false,
            times: [
              {
                from: '09:00',
                to: '18:00'
              }
            ]
          },
          {
            name: 'Студия на Академической',
            active: true,
            times: [
              {
                from: '09:00',
                to: '13:00'
              },
              {
                from: '15:00',
                to: '19:00'
              }
            ]
          },
          {
            name: 'Перерыв',
            active: false,
            times: [
              {
                from: '14:00',
                to: '15:00'
              }
            ]
          }
        ]
      },
      {
        name: 'Пятница',
        active: false,
        list: [
          {
            name: 'В Москве',
            active: false,
            times: [
              {
                from: '09:00',
                to: '18:00'
              }
            ]
          },
          {
            name: 'Студия на Академической',
            active: false,
            times: [
              {
                from: '10:00',
                to: '19:00'
              }
            ]
          },
          {
            name: 'Перерыв',
            active: false,
            times: [
              {
                from: '14:00',
                to: '15:00'
              }
            ]
          }
        ]
      },
    ],
    modalShow: false
  }

  render() {
    return (
      <div>
        {
          this.state.dayofweek.map(d =>
            <DayOfWeek
                name={d.name}
                active={d.active}
                list={d.list}
                changeActiveDay={this.changeActiveDay.bind(this, d.name)}
                changeActiveItem={this.changeActiveItem.bind(this, d.name)}
                changeScheduleTime={this.changeScheduleTime.bind(this, d.name)}
                addScheduleTime={this.addScheduleTime.bind(this, d.name)}
                removeScheduleTime={this.removeScheduleTime.bind(this, d.name)}
                key={d.name}
            />
          )
        }
        <button type="button" onClick={this.showState.bind(this)} className="btn btn-success btn-state">Показать состояние</button>
        <Modal show={this.state.modalShow} changeShow={this.changeModalState.bind(this)}>
          <pre>{JSON.stringify(this.state.dayofweek, null, '  ')}</pre>
        </Modal>
      </div>
    )
  }

  showState() {
    this.changeModalState()
  }

  changeModalState() {
    let newState = this.state;

    newState.modalShow = !newState.modalShow

    this.setState(newState)
  }

  addScheduleTime(dayName, itemName) {
    this.changeItem(dayName, itemName, item => {
      item.times.push({from: '00:00', to: '00:00'})

      return item
    })
  }

  changeScheduleTime(dayName, itemName, index, from, to) {
    this.changeItem(dayName, itemName, item => {
      item.times[index] = {from: from, to: to}

      return item
    })
  }

  removeScheduleTime(dayName, itemName, index) {
    this.changeItem(dayName, itemName, item => {
      item.times = item.times.filter((time, i) => index !== i)

      return item
    })
    console.log(this.state)
  }

  changeActiveDay(dayName) {
    this.setState(
      this.state.dayofweek.map(dayofweek => {
        if (dayofweek.name === dayName) {
          dayofweek.active = !dayofweek.active;
        }

        return dayofweek;
      })
    )
  }

  changeActiveItem(dayName, itemName) {
    this.changeItem(dayName, itemName, item => {
      item.active = !item.active

      return item
    })
  }

  changeItem(dayName, itemName, callback) {
    this.setState(
        this.state.dayofweek.map(dayofweek => {
          if (dayofweek.name === dayName) {
            dayofweek.list = dayofweek.list.map(item => item.name === itemName ? callback(item) : item)
          }

          return dayofweek;
        })
    )
  }
}

export default App;
