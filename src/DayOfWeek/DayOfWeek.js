import React from 'react'
import ScheduleDayOfWeek from './ScheduleDayOfWeek'

class DayOfWeek extends React.Component {
  render() {
    let scheduleDayOfWeek;

    if (this.props.active) {
      scheduleDayOfWeek = <ScheduleDayOfWeek
          list={this.props.list}
          changeActiveItem={this.props.changeActiveItem}
          changeScheduleTime={this.props.changeScheduleTime}
          addScheduleTime={this.props.addScheduleTime}
          removeScheduleTime={this.props.removeScheduleTime}
      />
    } else {
      scheduleDayOfWeek = null;
    }

    return (
      <div className="card dayofweek">
        <div className="card-header dayofweek-header">
          <b>{this.props.name}</b>
          <div className="form-check form-switch dayofweek-switcher-div">
            <input className="form-check-input dayofweek-switcher-input"
                   type="checkbox"
                   onChange={this.props.changeActiveDay}
                   defaultChecked={this.props.active}
            />
          </div>
        </div>
        {scheduleDayOfWeek}
      </div>
    )
  }
}

export default DayOfWeek;
