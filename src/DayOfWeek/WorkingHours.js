import React from 'react'

class WorkingHours extends React.Component {
  render() {
    const classes = ['bi']
    let onClick;

    if (this.props.num === this.props.size) {
      classes.push('bi-plus-circle-fill', 'dayofweek-time-add')
      onClick = this.props.addScheduleTime
    } else {
      classes.push('bi-x-circle-fill', 'dayofweek-time-delete')
      onClick = this.props.removeScheduleTime
    }

    const options = [...Array(24).keys()]
        .map(t => t.toString().padStart(2, '0') + ':00')
        .map(t => <option value={t} key={t}>{t}</option>)

    return (
      <li>
        <span>с</span>
        <select value={this.props.time.from} onChange={event => this.changeFrom(event.target.value, 'from')} className="form-select form-select-lg dayofweek-time-select">
          {options}
        </select>
        <span>до</span>
        <select value={this.props.time.to} onChange={event => this.changeFrom(event.target.value, 'to')} className="form-select form-select-lg dayofweek-time-select">
          {options}
        </select>
        <i className={classes.join(' ')} onClick={onClick} />
      </li>
    )
  }

  changeFrom(value, key) {
    let time = this.props.time;
    time[key] = value;
    this.props.changeScheduleTime(time.from, time.to)
  }
}

export default WorkingHours;
