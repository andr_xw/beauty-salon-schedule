import React from 'react'
import WorkingHours from './WorkingHours'

class ScheduleDayOfWeek extends React.Component {
  render() {
    return (
      <ul className="list-group list-group-flush dayofweek-list">
        {this.props.list.map(item => {
          return (
            <li className="list-group-item dayofweek-item" key={item.name}>
              <div className="form-check dayofweek-item-name">
                <input className="form-check-input dayofweek-item-checkbox"
                       type="checkbox"
                       defaultChecked={item.active}
                       onChange={() => this.props.changeActiveItem(item.name)} />
                <label className="form-check-label">{item.name}</label>
              </div>
              <ul className="dayofweek-time-block">
                {item.active ? item.times.map((time, i, a) =>
                  <WorkingHours time={time}
                                num={i + 1}
                                size={a.length}
                                addScheduleTime={() => this.props.addScheduleTime(item.name)}
                                removeScheduleTime={() => this.props.removeScheduleTime(item.name, i)}
                                changeScheduleTime={(from, to) => this.props.changeScheduleTime(item.name, i, from, to)}
                                key={i}
                  />) : null}
              </ul>
            </li>
          )
        })}
      </ul>
    )
  }
}

export default ScheduleDayOfWeek;
